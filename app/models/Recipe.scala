package models

import reactivemongo.bson._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._


case class Recipe(
  id: Option[BSONObjectID],
  name: String,
  slug: String,
  permalink: String,
  description: Option[String],
  instructions: Option[String],
  thumbnail_url: Option[String],
  ingredients: List[BSONObjectID])

object Recipe {
  implicit object RecipeBSONReader extends BSONDocumentReader[Recipe] {
    def read(doc: BSONDocument): Recipe =
      Recipe(
        doc.getAs[BSONObjectID]("_id"),
        doc.getAs[String]("name").get,
        doc.getAs[String]("slug").get,
        doc.getAs[String]("permalink").get,
        doc.getAs[String]("description"),
        doc.getAs[String]("instructions"),
        doc.getAs[String]("thumbnail_url"),
        doc.getAs[List[BSONObjectID]]("ingredients").toList.flatten
      )
  }

  implicit object RecipeBSONWriter extends BSONDocumentWriter[Recipe] {
    def write(recipe: Recipe): BSONDocument = BSONDocument(
      "_id" -> recipe.id.getOrElse(BSONObjectID.generate),
      "name" -> recipe.name,
      "slug" -> recipe.slug,
      "permalink" -> recipe.permalink,
      "description" -> recipe.description,
      "instructions" -> recipe.instructions,
      "thumbnail_url" -> recipe.thumbnail_url,
      "ingredients" -> recipe.ingredients
    )
  }

  val form = Form(
    mapping(
      "id" -> optional(of[String]),
      "name" -> nonEmptyText,
      "slug" -> nonEmptyText,
      "permalink" -> nonEmptyText,
      "description" -> optional(nonEmptyText),
      "instructions" -> optional(nonEmptyText),
      "thumbnail_url" -> optional(nonEmptyText),
      "ingredients" -> list(of[String])) { (id, name, slug, permalink, description, instructions, thumbnail_url, ingredients) =>
        Recipe(
          id.map(new BSONObjectID(_)),
          name,
          slug,
          permalink,
          description,
          instructions,
          thumbnail_url,
          ingredients.map(new BSONObjectID(_)))
      } { recipe =>
        Some(
          (recipe.id.map(_.stringify),
            recipe.name,
            recipe.slug,
            recipe.permalink,
            recipe.description,
            recipe.instructions,
            recipe.thumbnail_url,
            recipe.ingredients.toList.map(_.stringify)))
      })
}
