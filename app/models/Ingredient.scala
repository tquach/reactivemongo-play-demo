package models

import reactivemongo.bson.{BSONDocumentWriter, BSONObjectID, BSONDocumentReader, BSONDocument}

case class Ingredient(
  id: Option[BSONObjectID],
  name: String,
  description: String,
  category: String)

object Ingredient {
  implicit object IngredientBSONReader extends BSONDocumentReader[Ingredient] {
    def read(doc: BSONDocument): Ingredient =
      Ingredient(
        doc.getAs[BSONObjectID]("_id"),
        doc.getAs[String]("name").get,
        doc.getAs[String]("description").get,
        doc.getAs[String]("category").get
      )
  }

  implicit object IngredientBSONWriter extends BSONDocumentWriter[Ingredient] {
    def write(ingredient: Ingredient): BSONDocument = BSONDocument(
      "_id" -> ingredient.id.getOrElse(BSONObjectID.generate),
      "name" -> ingredient.name,
      "description" -> ingredient.description,
      "category" -> ingredient.category
    )
  }
}
