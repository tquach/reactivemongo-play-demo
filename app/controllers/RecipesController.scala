package controllers

import scala.concurrent.Future

import play.api.Play.current
import play.api.mvc._

import reactivemongo.bson._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.collections.default.BSONCollection
import play.api.Logger
import models.Recipe

object RecipesController extends Controller with MongoController {

  val collection = db[BSONCollection]("recipes")

  def list = Action { implicit request =>
    Async {
      val query = BSONDocument("$query" -> BSONDocument())

      val found = collection.find(query).cursor[Recipe]

      found.toList.map { recipes =>
        Ok(views.html.recipes(recipes))
      }.recover {
        case e =>
          Logger.error(e.getStackTraceString)
          BadRequest(e.getMessage())
      }
    }
  }

  def create = Action {
    Ok(views.html.recipe_form(None, Recipe.form))
  }

  def save = Action { implicit request =>
    Recipe.form.bindFromRequest.fold(
      errors => Ok(views.html.recipe_form(None, errors)),

      recipe => AsyncResult {
        collection.insert(recipe).map(_ =>
          Redirect(routes.RecipesController.list()))
      }
    )
  }

  def find(slug: String) = Action { implicit request =>
    Async {
      val cursor = collection.find(BSONDocument("slug" -> slug)).cursor[Recipe]

      val futureList: Future[List[Recipe]] = cursor.toList

      futureList.map { list =>
        Ok(views.html.recipe_detail(Some(list(0))))
      }.recover {
        case e =>
          BadRequest(e.getMessage())
      }
    }
  }


}
