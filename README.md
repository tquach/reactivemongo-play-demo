Recipe Centre Proof of Concept
==============================

This Play application uses ReactiveMongo to demonstrate persistence to mongodb.

Requirements
============
* An instance of mongodb server running.

Configuration
=============
* Update the application.conf with the mongodb server settings.